<?php

namespace SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter\Api;

class PoloniexAPI extends \ccxt\poloniex
{
    public function fetch_deposit_address ($code, $params = array ()) {
        $response = $this->privatePostReturnDepositAddresses();
        $address = $response[$code];
        $this->check_address($address);
        $status = $address ? 'ok' : 'none';
        return array (
            'currency' => $code,
            'address' => $address,
            'status' => $status,
        );
    }

    public function nonce()
    {
        return $this->microseconds();
    }
}