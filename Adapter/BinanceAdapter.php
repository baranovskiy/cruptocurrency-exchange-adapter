<?php

namespace SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter;

use SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Coin\BalanceCoin;

class BinanceAdapter extends ExchangeAdapter
{
    const EXCHANGE = 'BINANCE';

    /**
     * @return BalanceCoin[]
     */
    public function getBalance(): array
    {
        $coins = [];

        foreach ($this->client->fetch_balance()['info']['balances'] as $handle => $coinData) {
            $coins[$coinData['asset']] = new BalanceCoin(
                static::EXCHANGE,
                $coinData['asset'],
                $coinData['free'],
                $coinData['locked']
            );
        }

        return $coins;
    }
}