<?php

namespace SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter;

use SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Coin\BalanceCoin;

class BittrexAdapter extends ExchangeAdapter
{
    const EXCHANGE = 'BITTREX';

    /**
     * @return BalanceCoin[]
     */
    public function getBalance(): array
    {
        $coins = [];
        foreach ($this->client->fetch_balance()['info'] as $handle => $coinData) {

            $coins[$coinData['Currency']] = new BalanceCoin(
                static::EXCHANGE,
                $coinData['Currency'],
                $coinData['Available'],
                $coinData['Pending']
            );
        }

        return $coins;
    }
}