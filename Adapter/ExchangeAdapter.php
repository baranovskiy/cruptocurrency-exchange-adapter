<?php

namespace SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter;

use SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter\Exceptions\OrderException;
use SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter\Interfaces\ExchangeAdapterInterface;
use SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter\Order\Order;
use SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter\Order\OrderStatus;
use SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Coin\BalanceCoin;
use SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Coin\BasicPair;
use ccxt\poloniex;
use ccxt\Exchange;

abstract class ExchangeAdapter implements ExchangeAdapterInterface
{
    const EXCHANGE = 'EXCHANGE';

    const CLASS_PATH = 'App\\Exchange\\Adapter\\Api\\%sAPI';

    /**
     * @var poloniex|Exchange
     */
    protected $client;

    /**
     * @param string $apiKey
     * @param string $secret
     */
    public function __construct(string $apiKey, string $secret)
    {
        $exchangeHandle = mb_convert_case($this->getExchangeHandle(), MB_CASE_TITLE, "UTF-8");
        $className = \sprintf(self::CLASS_PATH, $exchangeHandle);

        $this->client = new $className(['apiKey' => $apiKey, 'secret' => $secret]);
    }

    /**
     * @return BasicPair[]
     *
     * @throws \ccxt\NotSupported
     */
    public function getTicker(): array
    {
        $coins = [];

        foreach ($this->client->fetch_tickers() as $pair => $coinData) {
            $handle = \explode('/', $coinData['symbol']);

            $coins[$pair] = new BasicPair(
                $this->getExchangeHandle(),
                $handle[0],
                $coinData['last'],
                $coinData['percentage'] ?? 0,
                $coinData['symbol']
            );
        }

        return $coins;
    }

    /**
     * @param string $symbol
     *
     * @return string
     *
     * @throws \ccxt\NotSupported
     */
    public function getAddress(string $symbol): string
    {
        $addressInfo = $this->client->fetch_deposit_address($symbol);

        return $addressInfo['address'];
    }

    /**
     * @param string $orderId
     * @param string $symbol
     * @return OrderStatus
     * @throws OrderException
     */
    public function getOrderStatus(string $orderId, string $symbol): OrderStatus
    {
        $status = $this->client->fetch_order_status($orderId, $symbol . '/BTC');

        if ($status) {
            return new OrderStatus($orderId, $status);
        }

        throw new OrderException('Order status failed.');
    }

    /**
     * @param string $coinHandle
     * @param float $amount
     * @param float $rate
     *
     * @return Order
     *
     * @throws OrderException
     */
    public function buy(string $coinHandle, float $amount, float $rate): Order
    {
        $data = $this->client->create_limit_buy_order($coinHandle . '/BTC', $amount, $rate);

        if (isset($data['id'])) {
            return new Order($coinHandle, Order::ORDER_BUY, $data['id'], $amount, $rate);
        }

        throw new OrderException('Order buy failed.');
    }

    /**
     * @param string $coinHandle
     * @param float $amount
     * @param float $rate
     *
     * @return Order
     *
     * @throws OrderException
     */
    public function sell(string $coinHandle, float $amount, float $rate): Order
    {
        $data = $this->client->create_limit_sell_order($coinHandle. '/BTC', $amount, $rate);

        if (isset($data['id'])) {
            return new Order($coinHandle, Order::ORDER_SELL, $data['id'], $amount, $rate);
        }

        throw new OrderException('Order sell failed.');
    }

    /**
     * @param string $orderUuid
     * @param string $coinHandle
     *
     * @return bool
     *
     * @throws \ccxt\CancelPending
     * @throws \ccxt\NotSupported
     */
    public function cancelOrder(string $orderUuid, string $coinHandle): bool
    {
        $data = $this->client->cancel_order($orderUuid, $coinHandle . '/BTC');

        return (bool) $data['success'];
    }

    /**
     * @return string
     */
    public function getExchangeHandle(): string
    {
        return static::EXCHANGE;
    }


    /**
     * @return BalanceCoin[]
     */
    abstract public function getBalance(): array;
}