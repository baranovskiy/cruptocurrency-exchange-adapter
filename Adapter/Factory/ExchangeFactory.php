<?php

namespace SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter\Factory;

use SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter\ExchangeAdapter;
use SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter\Interfaces\ExchangeAdapterInterface;

class ExchangeFactory
{
    /**
     * @var ExchangeAdapter
     */
    private $adapters;

    /**
     * @param ExchangeAdapterInterface $adapter
     */
    public function addAdapter(ExchangeAdapterInterface $adapter)
    {
        $this->adapters[$adapter->getExchangeHandle()] = $adapter;
    }

    /**
     * @param string $handle
     * @return ExchangeAdapterInterface|null
     */
    public function getByHandle(string $handle): ?ExchangeAdapterInterface
    {
        return isset($this->adapters[$handle]) ? $this->adapters[$handle] : null;
    }
}