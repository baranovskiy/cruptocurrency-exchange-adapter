<?php

namespace SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter\Interfaces;

use SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter\Order\Order;
use SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter\Order\OrderStatus;
use SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Coin\BalanceCoin;
use SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Coin\BasicPair;

interface ExchangeAdapterInterface
{
    /**
     * @return BalanceCoin[]
     */
    public function getBalance(): array;

    /**
     * @return BasicPair[]
     */
    public function getTicker(): array;

    /**
     * @param string $symbol
     *
     * @return string
     */
    public function getAddress(string $symbol): string;

    /**
     * @return string
     */
    public function getExchangeHandle(): string;

    /**
     * @param string $coinHandle
     * @param float $amount
     * @param float $rate
     *
     * @return Order
     */
    public function buy(string $coinHandle, float $amount, float $rate): Order;

    /**
     * @param string $coinHandle
     * @param float $amount
     * @param float $rate
     *
     * @return Order
     */
    public function sell(string $coinHandle, float $amount, float $rate): Order;

    /**
     * @param string $orderId
     * @param string $symbol
     *
     * @return OrderStatus
     */
    public function getOrderStatus(string $orderId, string $symbol): OrderStatus;
}