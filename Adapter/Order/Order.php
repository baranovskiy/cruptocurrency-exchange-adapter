<?php

namespace SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter\Order;

class Order
{
    const ORDER_SELL = 'SELL';
    const ORDER_BUY = 'BUY';

    /**
     * @var string
     */
    protected $orderId;

    /**
     * @var string
     */

    protected $type;

    /**
     * @var float
     */
    protected $amount;

    /**
     * @var float
     */
    protected $rate;

    /**
     * @var string
     */
    protected $coinHandle;

    /**
     * @param string $coinHandle
     * @param string $orderId
     * @param string $type
     * @param float $amount
     * @param float $rate
     */
    public function __construct(
        string $coinHandle,
        string $orderId,
        string $type,
        float $amount,
        float $rate
    ) {
        $this->coinHandle = $orderId;
        $this->orderId = $orderId;
        $this->amount = $amount;
        $this->rate = $rate;
    }

    /**
     * @return string
     */
    public function getCoinHandle(): string
    {
        return $this->coinHandle;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return float
     */
    public function getRate(): float
    {
        return $this->rate;
    }

    /**
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->orderId;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}