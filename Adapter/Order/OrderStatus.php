<?php

declare(strict_types=1);

namespace SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter\Order;

class OrderStatus
{
    private const CLOSED_STATUS = 'closed';

    /**
     * @var string
     */
    private $orderId;

    /**
     * @var string
     */
    private $status;

    /**
     * @param string $orderId
     * @param string $status
     */
    public function __construct(string $orderId, string $status)
    {
        $this->orderId = $orderId;
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->orderId;
    }

    /**
     * @return bool
     */
    public function isClosed(): bool
    {
        return $this->status === self::CLOSED_STATUS;
    }
}