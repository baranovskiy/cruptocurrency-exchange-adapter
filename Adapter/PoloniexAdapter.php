<?php

namespace SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter;

use SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Coin\BalanceCoin;

class PoloniexAdapter extends ExchangeAdapter
{
    const EXCHANGE = 'POLONIEX';

    /**
     * @return BalanceCoin[]
     */
    public function getBalance(): array
    {
        $coins = [];

        foreach ($this->client->fetch_balance()['info'] as $handle => $coinData) {
            $coins[$handle] = new BalanceCoin(
                static::EXCHANGE,
                $handle,
                $coinData['available'],
                $coinData['onOrders']
            );
        }

        return $coins;
    }
}