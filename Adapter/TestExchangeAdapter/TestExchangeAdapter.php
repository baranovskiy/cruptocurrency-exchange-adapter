<?php

declare(strict_types=1);

namespace SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter\TestExchangeAdapter;

use SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter\Interfaces\ExchangeAdapterInterface;
use SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter\Order\Order;
use SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Adapter\Order\OrderStatus;
use SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Coin\BalanceCoin;
use SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Coin\BasicPair;

class TestExchangeAdapter implements ExchangeAdapterInterface
{
    const EXCHANGE = 'EXCHANGE';
    const CLASS_PATH = 'SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\\\Adapter\\Exchange\\%sAPI';

    private $handle;
    private $coinsHandle = ['BCN', 'BTS', 'BURST', 'CLAM', 'DASH', 'DGB', 'DOGE', 'GAME', 'HUC', 'LTC'];

    /**
     * @return BasicPair[]
     */
    public function getTicker(): array
    {
        $coins = [];

        foreach ($this->coinsHandle as $coin) {
            $coins[] = new BasicPair(
                $this->handle.'/BTC',
                $coin,
                $this->getRandom(0.00000036, 0.00930668, 100000000),
                $this->getRandom(-3.4474, 5.8824, 10000),
                'BTC'
            );
        }

        return $coins;
    }

    /**
     * @param string $symbol
     *
     * @return string
     */
    public function getAddress(string $symbol): string
    {
        return md5(uniqid());
    }

    /**
     * @param string $orderId
     * @param string $symbol
     *
     * @return OrderStatus
     */
    public function getOrderStatus(string $orderId, string $symbol): OrderStatus
    {
        $status = ["open", "closed"];

        return new OrderStatus($orderId, $status[array_rand($status, 1)]);
    }

    /**
     * @param string $coinHandle
     * @param float $amount
     * @param float $rate
     *
     * @return Order
     */
    public function buy(string $coinHandle, float $amount, float $rate): Order
    {
        return new Order(
            $coinHandle,
            md5(uniqid()),
            Order::ORDER_BUY,
            $amount,
            $rate
        );
    }

    /**
     * @param string $coinHandle
     * @param float $amount
     * @param float $rate
     *
     * @return Order
     */
    public function sell(string $coinHandle, float $amount, float $rate): Order
    {
        return new Order(
            $coinHandle,
            md5(uniqid()),
            Order::ORDER_SELL,
            $amount,
            $rate
        );
    }

    /**
     * @param string $orderUuid
     * @param string $coinHandle
     *
     * @return bool
     */
    public function cancelOrder(string $orderUuid, string $coinHandle): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getExchangeHandle(): string
    {
        return $this->handle;
    }

    /**
     * @param $min
     * @param $max
     * @param $div
     *
     * @return float|int
     */
    private function getRandom($min, $max, $div)
    {
        $min = $min * $div;
        $max = $max * $div;

        return mt_rand($min, $max) / $div;
    }

    public function setHandle($handle)
    {
        $this->handle = $handle;
    }


    /**
     * @return BalanceCoin[]
     */
    public function getBalance(): array
    {
        $coins = [
            new BalanceCoin($this->handle, "TEST", rand(1, 10), rand(1, 10)),
            new BalanceCoin($this->handle, "LOL", rand(1, 10), rand(1, 10)),
            new BalanceCoin($this->handle, "KEK", rand(1, 10), rand(1, 10)),
            new BalanceCoin($this->handle, "CHEBUREK", rand(1, 10), rand(1, 10)),
        ];

        return $coins;
    }
}