<?php

namespace SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Coin;

class BalanceCoin
{
    /**
     * @var string
     */
    private $exchange;

    /**
     * @var string
     */
    private $handle;

    /**
     * @var float
     */
    private $balance;

    /**
     * @var float
     */
    private $lockedBalance;

    /**
     * @param string $exchange
     * @param string $handle
     * @param float $balance
     * @param float $lockedBalance
     */
    public function __construct(string $exchange, string $handle, float $balance, float $lockedBalance)
    {
        $this->exchange = $exchange;
        $this->handle = $handle;
        $this->balance = $balance;
        $this->lockedBalance = $lockedBalance;
    }

    /**
     * @return string
     */
    public function getExchange(): string
    {
        return $this->exchange;
    }

    /**
     * @return string
     */
    public function getHandle(): string
    {
        return $this->handle;
    }

    /**
     * @return float
     */
    public function getLockedBalance(): float
    {
        return $this->lockedBalance;
    }

    /**
     * @return float
     */
    public function getBalance(): float
    {
        return $this->balance;
    }
}