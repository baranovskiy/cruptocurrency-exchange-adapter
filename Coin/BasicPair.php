<?php

namespace SkySoft\CryptoCurrencyBundle\CryptoCurrencyExchangeBundle\Coin;

class BasicPair
{
    /**
     * @var string
     */
    private $exchange;

    /**
     * @var string
     */
    private $handle;

    /**
     * @var float
     */
    private $rate;

    /**
     * @var float
     */
    private $percentage24h;

    /**
     * @var string
     */
    private $pair;

    /**
     * @param string $exchange
     * @param string $handle
     * @param float $rate
     * @param float $percentage24h
     * @param string $pair
     */
    public function __construct(string $exchange, string $handle, float $rate, float $percentage24h, string $pair)
    {
        $this->exchange = $exchange;
        $this->handle = $handle;
        $this->rate = $rate;
        $this->pair = $pair;
        $this->percentage24h = $percentage24h;
    }

    /**
     * @return string
     */
    public function getExchange(): string
    {
        return $this->exchange;
    }

    /**
     * @return string
     */
    public function getHandle(): string
    {
        return trim($this->handle);
    }

    /**
     * @return float
     */
    public function getRate(): float
    {
        return $this->rate;
    }

    /**
     * @return string
     */
    public function getPair(): string
    {
        return $this->pair;
    }

    /**
     * @param string $pair
     *
     * @return bool
     */
    public function isPair(string $pair): bool
    {
        return $this->pair === $this->handle . '/' . $pair;
    }

    /**
     * @return float
     */
    public function get24hPercentageChange(): float
    {
        return round($this->percentage24h, 4);
    }
}